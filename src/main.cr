require "cray"
require "logger"
require "./core/pages"
require "./pages/main_menu"
require "./core/utils"

DEBUG = true

def init
  LibRay.set_config_flags LibRay::FLAG_WINDOW_RESIZABLE
  LibRay.init_window Uni::WIDTH, Uni::HEIGHT, "DAMA"
  LibRay.set_target_fps 200
  Pages.new_and_set(MainMenu.new())
end


init
last_delta = 1.0
while !LibRay.window_should_close?
  LibRay.begin_drawing

  LibRay.draw_fps 0, 0 if DEBUG
  delta = LibRay.get_frame_time
  last_delta += delta

  # Draw
  if last_delta > Uni::DRAW_DELTA
    LibRay.clear_background LibRay::BLACK
    last_delta = 0.0
    Pages.current.draw
  end

  # Update
  Pages.current.update(delta)

  LibRay.end_drawing
end

LibRay.close_window


