
class ViewArea < Area
  def initialize (@farea : FirstArea)
    super()
  end

  def update(dt)
    super
    @farea.update(0.05*dt)
    if LibRay.key_down? LibRay::KEY_ENTER
      ar = MainMenu.new
      GC.collect
      Pages.new_and_set ar
    end
  end

  def draw
    super
    @farea.draw()
    LibRay.draw_text "GAME OVER", Uni::WIDTH /2 - 32, Uni::HEIGHT / 2 - 40, 40, LibRay::WHITE
    LibRay.draw_text @farea.score.to_s, Uni::WIDTH /2 - 32, Uni::HEIGHT / 2 + 40, 32, LibRay::WHITE
    LibRay.draw_text "PRESS ENTER TO RESTART", Uni::WIDTH /2 - 32, Uni::HEIGHT / 2 + 80, 24, LibRay::WHITE
  end

  def next

  end

end