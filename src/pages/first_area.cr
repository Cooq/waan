require "../core/area"
require "../objects/vessel"
require "../objects/asteroid"
require "../core/spawner"
require "../core/utils"
require "./view_area"

class FirstArea < Area
  def initialize (@vessel : Vessel)
    super()
    @vessel.position = LibRay::Vector2.new(x:  Uni::WIDTH/2, y: Uni::HEIGHT_f/2)
    func = ->(x : Float64, y : Float64){ Asteroid.new self, LibRay::Vector2.new(x: x, y: y); return }
    Spawner.new self, 0.0..Uni::WIDTH_f, -100.0..-50.0, 0.1..1.0, func
    Spawner.new self, 0.0..Uni::WIDTH_f, (Uni::HEIGHT_f+50)..(Uni::HEIGHT_f+100), 0.1..1.0, func
    Spawner.new self, -100.0..-50.0, 0.0..Uni::HEIGHT_f, 0.1..1.0, func
    Spawner.new self, (Uni::WIDTH_f+50)..(Uni::WIDTH_f+100), 0.0..Uni::HEIGHT_f, 0.1..1.0, func
    f = ->(x : Float64, y : Float64){ p = Particle.new self, LibRay::Vector2.new(x: x, y: y), Math::PI, -40, 60.0, Uni.randomf(2.0); p.color(225, 255,200,200); return }
    Spawner.new self, 0.0..Uni::WIDTH_f, 0.0..0.0, 0.1..1.0, f
    100.times do
      f.call(Uni.randomf(0.0..Uni::WIDTH_f), Uni.randomf(0.0..Uni::HEIGHT_f))
    end
  end

  def update(dt)
    super
    if LibRay.key_down? LibRay::KEY_Q
      Pages.speed = 0.1
    else
      Pages.speed = 1.0
    end
    @vessel.dy = dt * Uni::SPEED
    # p @game_objects
  end

  def draw
    super
    LibRay.draw_text @score.to_s, Uni::WIDTH - 100, 20, 16, LibRay::WHITE
    LibRay.draw_text "go_size : #{@game_objects.size}", Uni::WIDTH - 100, 40, 16, LibRay::WHITE
  end

  def next (sym : Symbol)
    if sym == :dead
      Pages.new_and_set (ViewArea.new self)
    end
  end

end