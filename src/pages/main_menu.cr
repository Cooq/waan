require "../objects/vessel"
require "./first_area"

class MainMenu < Area
  @vessel = Vessel.new Area.new, LibRay::Vector2.new(x: 600, y: 500), 0 # Bad fix
  @typ = 0
  @rotation = Math::PI


  def initialize
    super
    create_vessel
  end

  def create_vessel
    @vessel = Vessel.new self, LibRay::Vector2.new(x: 600, y: 500), @typ
    @vessel.rotation = @rotation
  end

  def update(dt)
    super
    if LibRay.key_pressed? LibRay::KEY_N
      @vessel.die
      @typ = (@typ + 1) % Vessel::MAX
      @rotation = @vessel.rotation
      create_vessel
    end
    if LibRay.key_pressed? LibRay::KEY_ENTER
      self.next :next
    end
  end

  def draw
    super
    LibRay.draw_text "WAAN", Uni::WIDTH /2 - 32, Uni::HEIGHT / 2 - 60, 50, LibRay::WHITE
    LibRay.draw_text "PRESS N TO CHANGE VESSEL", Uni::WIDTH /2 - 32, Uni::HEIGHT / 2 + 40, 32, LibRay::WHITE
  end

  def next(sym : Symbol)
    if sym == :next
      v = @vessel
      new_area = FirstArea.new @vessel
      @vessel.area = new_area
      Pages.new_and_set new_area
    end
  end

end