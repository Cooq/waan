require "../core/game_object"
require "../core/utils"

class Particle < GameObject
  @position = LibRay::Vector2.new(x: 0.0, y: 0.0)
  @rotation = 0.0
  property rotation, radius, r, g, b, a
  @radius = 5.0
  @r = 255
  @g = 255
  @b = 255
  @a = 255

  def initialize(@area, @position, @rotation, speed, time)
    super(@area, @position)
    @v = @position
    self.speed= speed
    self.timer.after time, ->{self.die}
  end

  def initialize(@area, @position, @rotation, speed, time, @radius)
    initialize(@area, @position, @rotation, speed, time)
  end
  def initialize(@area, @position, @rotation, speed, time, @radius, @collision)
    initialize(@area, @position, @rotation, speed, time)
  end

  def speed=(v)
    x = Math.sin(@rotation)
    y = Math.cos(@rotation)
    n = Math.sqrt (x**2 + y**2)
    @v = LibRay::Vector2.new(x: x/n * v, y: y/n * v)
  end

  def draw
    LibRay.draw_circle_lines @position.x, @position.y, @radius, LibRay::Color.new(r: @r, g: @g, b: @b, a: @a)
  end

  def update(dt)
    super
    @position = @position + @v * dt
    die if @radius < 1e-5
  end

  def color(r, g, b, a = 255)
    @r = r
    @g = g
    @b = b
    @a = a
  end

  def rand_color
    color(Uni.random(255), Uni.random(255), Uni.random(255))
  end

end
