require "../core/game_object"
require "../core/utils"
require "./particle"
require "./bullet"
require "./multipoint"
require "./asteroid"
require "./weapon"

class Vessel < MultiPoint
  @fire_point = 0
  @fuel_point = 0
  @fire_reset = 0.0
  @typ = 0
  @weapon : Weapon

  VESSEL_MODELS =
  [[
    LibRay::Vector2.new(x: 0, y: 0),
    LibRay::Vector2.new(x: 20, y: 80),
    LibRay::Vector2.new(x: 40, y: 0),
    LibRay::Vector2.new(x: 20, y: 10)],
  [
    LibRay::Vector2.new(x: 0, y: 0),
    LibRay::Vector2.new(x: 0, y: 70),
    LibRay::Vector2.new(x: 10, y: 80),
    LibRay::Vector2.new(x: 10, y: 40),
    LibRay::Vector2.new(x: 25, y: 40),
    LibRay::Vector2.new(x: 40, y: 40),
    LibRay::Vector2.new(x: 40, y: 80),
    LibRay::Vector2.new(x: 50, y: 70),
    LibRay::Vector2.new(x: 50, y: 0),
    LibRay::Vector2.new(x: 25, y: 0)],
    [
      LibRay::Vector2.new(x: 0, y: 0),
    LibRay::Vector2.new(x: 20, y: 40),
    LibRay::Vector2.new(x: 40, y: 0),
    LibRay::Vector2.new(x: 20, y: 10)],
  ]
  MAX = VESSEL_MODELS.size
  VESSEL_MODELS_POINTS =
  [ {:fire_point => 1, :fuel_point =>3},
  {:fire_point => 4, :fuel_point =>9},
  {:fire_point => 1, :fuel_point =>3},
  ]

  def initialize(@area, @position, @typ = 0)
    super(@area, @position)
    @weapon = Weapon::DATA[@typ]
    set_model typ
    # fuel
    @timer.every(0.02, ->{self.fuel})
    # @timer.after 0.2, ->{die}
  end

  def set_model(model_type)
    add_point VESSEL_MODELS[model_type]
    @fire_point = VESSEL_MODELS_POINTS[model_type][:fire_point]
    @fuel_point = VESSEL_MODELS_POINTS[model_type][:fuel_point]
    @weapon = Weapon::DATA[model_type]
    @timer.every(@weapon.delay, ->{self.fire})
  end

  def set_model(model_type : String)
    super
    fuel
  end

  def fire : Void
    @fire_reset = 0.0
    @weapon.call(@points[@fire_point], -Math::PI*2 - @rotation, @area)
  end

  def fuel : Void
    spawn_life = 0.1 + Uni.randomf(0.3)
    npoint = LibRay::Vector2.new(x: Uni.random(-10..10),y: -10)
    npoint = npoint.rotate(@rotation)
    npoint = npoint.+(@points[@fuel_point])
    # if @dx != 0 || @dy != 0
    #   f = Particle.new area: @area, position: npoint, rotation: - @rotation, speed: -10.0, time: spawn_life
    # else
      f = Particle.new area: @area, position: npoint, rotation: - @rotation, speed: -200, time: spawn_life
    # end
    f.b = 20
    f.g = 150
    f.timer.tween(spawn_life, f.radius, 0.0, ->(r : Float64){f.radius -= r; return})
    f.timer.tween(spawn_life, f.r, 100.0, ->(r : Float64){f.r -= r.to_i; return})
    f.timer.tween(spawn_life, f.g, 0.0, ->(r : Float64){f.g -= r.to_i; return})
    f.timer.tween(spawn_life, f.b, 0.0, ->(r : Float64){f.b -= r.to_i; return})
  end

  def update(dt)
    super
    if LibRay.key_down? LibRay::KEY_W
      self.dy = dt * Uni::SPEED
    end
    if LibRay.key_down? LibRay::KEY_S
      self.dy = -dt * Uni::SPEED
    end
    if LibRay.key_down? LibRay::KEY_A
      self.rotation = -dt * 8
    elsif LibRay.key_down? LibRay::KEY_D
      self.rotation = dt * 8
    end
    if LibRay.key_down? LibRay::KEY_SPACE
      self.fire
    end
    if LibRay.key_down? LibRay::KEY_C
      LibRay.take_screenshot("test")
    end
    LibRay.draw_text "%0.1f %0.1f" % [@points[@fire_point].x, @points[@fire_point].y], 500, 0, 20, LibRay::WHITE if DEBUG
    @area.each do |other|
      if other.is_a? Asteroid
        if other.collision == :enemy && LibRay.check_collision_point_circle @position, other.position, other.radius
          # @timer.during 2.0, ->(t : Float64){LibRay.draw_text "deadly dead", 0, 0, 80, LibRay::WHITE;}
          die
          break
        end
      end
    end
  end

  def die
    if ! dead?
      super
      # @area.die
      ti = 360
      ti.times do |t|
        Bullet.new @area, @points[@fire_point], t*2*Math::PI/ti, 2000, 5.0, 5.0, :vessel, damage: 1.0
        p = Particle.new @area, @points[@fire_point], Uni.randomf(-Math::PI..Math::PI), Uni.randomf(200), Uni.randomf(2.5), Uni.randomf(2.5)
        p.rand_color
      end
      @area.next :dead
    end
    return
  end
end
