require "../core/game_object"
require "../core/utils"
require "./particle"

class MultiPoint < GameObject
  @points = [] of LibRay::Vector2
  @rotation = 0.0
  @dx = 0.0
  @dy = 0.0
  @vx = 0.0
  RESET = 0.1
  RESET_FUEL = 0.1
  @reset_count = 10.0
  @color = LibRay::Color.new(r: 255, g:255, b:255, a: 255)
  property rotation

  def initialize(@area : Area, @position : LibRay::Vector2)
    super(@area, @position)
    @dr = @rotation
  end

  def dy=(other_value : Float)
    @dy += other_value.to_f
  end

  def dx=(other_value : Float)
    @dx += other_value.to_f
  end

  def update_coordinates
    @position = @position.+(@dx, @dy, @rotation)
    @points = @points.map do |point|
      point = point.+(@dx, @dy, @rotation)
    end
  end

  def update_rotation
    @points = @points.map do |point|
      point.rotate(@position, @dr)
    end
  end

  def rotation=(other_value : Float)
    @dr = other_value.to_f
    @rotation += other_value.to_f
  end

  def add_point(v : LibRay::Vector2)
    @points << v + @position
  end

  def add_point(v : Array(LibRay::Vector2))
    v.each do |u| add_point u end
  end

  def compute_position
    @position = @points.reduce{ |a, b| a + b}
    @position /= @points.size
    @reset_count = 0.0
  end

  def draw
    super
    sz = @points.size
    if sz > 1
      @points.each_with_index do |point, index|
        other_point = @points[index + 1]? || @points[0]
        LibRay.draw_line_v(point, other_point, @color)
      end
    end
  end

  def update(dt)
    super
    compute_position
    @reset_count = 0.0 if @reset_count > RESET
    @reset_count += dt
    # @position = @position.+(@dx, @dy, @rotation)
    update_coordinates
    update_rotation
    @dx = 0.0
    @dy = 0.0
    @dr = 0.0
  end

end
