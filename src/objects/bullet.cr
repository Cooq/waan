require "../core/game_object"
require "../core/utils"

class Bullet < Particle
  CLASSES = [Asteroid]
  @damage = 0.0

  def initialize(@area, @position, @rotation, speed, time, @radius, @collision, @damage)
    super(@area, @position, @rotation, speed, time)
  end

  def update(dt)
    super
    @area
    .each do |other|
      if other.is_a? Asteroid && !other.dead? && LibRay.check_collision_circles(other.position, other.radius, @position, @radius)
        other.hit(@damage)
        die
        Pages.current.area.score += (50 / @radius).to_i
        break
      end
    end
  end

end
