struct Weapon
  DATA = [
    Weapon.new(f: ->(pos : LibRay::Vector2, r : Float64, area : Area) {
      Bullet.new area: area, position: pos, rotation: r, speed: Uni::SPEED *3, time: 3.0, radius: 5.0, collision: :vessel, damage: 1.0
      return
    }, delay: 0.1, damage: 1.0),
    Weapon.new(f: ->(pos : LibRay::Vector2, r : Float64, area : Area) {
      a = 10
      a.times do |t|
        Bullet.new area: area, position: pos, rotation: r - (t - (a-1)/2) * 0.05, speed: Uni::SPEED * 3, time: 3.0, radius: 5.0, collision: :vessel, damage: 1.0
      end
      return
    }, delay: 0.5, damage: 1.0),
    Weapon.new(f: ->(pos : LibRay::Vector2, r : Float64, area : Area) {
      a = 20
      a.times do |t|
        Bullet.new area: area, position: pos, rotation: r - (t - (a-1)/2) * 0.05, speed: Uni::SPEED * 1.5, time: 0.8, radius: 5.0, collision: :vessel, damage: 1.0
      end
      return
    }, delay: 0.2, damage: 1.0),
  ]
  property f
  property delay
  property damage
  def initialize(@f : Proc(LibRay::Vector2, Float64, Area, Nil), @delay : Float64, @damage : Float64); end
  def call(a, b, c)
    @f.call(a, b, c)
  end
end
