require "./multipoint"
require "../core/utils"

class Asteroid < MultiPoint
  MAX_POINT = 32
  MIN_POINT = 8
  MIN_SIZE = 20
  MAX_SIZE = 50

  @v : LibRay::Vector2
  @rotation = Uni.randomf(2*Math::PI)
  @radius = Uni.randomf(MIN_SIZE..MAX_SIZE)
  property radius

  def initialize (@area, @position)
    super
    @v = @position
    count = Uni.random(MIN_POINT..MAX_POINT)
    angle = 2*Math::PI / count
    r = 3*@radius/4..@radius
    @life = get_init_life
    count.times do | n |
      radius = Uni.random r
      point = LibRay::Vector2.new(x:0, y: radius)
      point = point.rotate(LibRay::Vector2.new(x: 0, y: 0), angle * n)
      @points << point + @position
    end
    self.speed = Uni::SPEED * (Uni.random() + 0.5)
    @collision = :enemy
    # @timer.after(2.0, ->{self.die})
  end

  def initialize (@area, @position, @rotation)
    initialize(@area, @position)
  end

  def initialize (@area, @position, @rotation, @radius : Float64)
    initialize(@area, @position)
  end

  def get_init_life
    @radius/10
  end

  def hit(damage)
    super
    @color = LibRay::Color.new(g: @life/get_init_life*255, b: @life/get_init_life*255, r: 255, a: 255)
  end

  def speed=(v)
    x = Math.sin(@rotation)
    y = Math.cos(@rotation)
    n = Math.sqrt (x**2 + y**2)
    @v = LibRay::Vector2.new(x: x/n * v, y: y/n * v)
  end

  def draw
    super
    LibRay.draw_text "#{@radius} - #{@life}", @position.x, @position.y + 20, 8, LibRay::WHITE if DEBUG
  end

  def update(dt)
    super
    @points = @points.map do |point|
      point = point + @v * dt
    end
    if @position.x > 2*Uni::WIDTH || @position.x < -2*Uni::WIDTH || @position.y > 2 * Uni::HEIGHT || @position.y < -2 * Uni::HEIGHT
      @cannot_explode = true
      die
    end
  end

  def die
    super
    10.to_i.times do
      rotation = Uni.random(-Math::PI..Math::PI)
      p = Particle.new area: @area, position: @position, rotation: rotation, speed: Uni.randomf(200), time: Uni.randomf(0.0..2.0), radius: 2.0
      p.color(Uni.random(200..255),Uni.random(150..200),Uni.random(50..150))
      t = Uni.randomf(@radius)
      p.position= p.position.+(t, t, -rotation)
    end
    unless @cannot_explode
      if @radius > MAX_SIZE / 4
        Uni.random(2..3).times do
          Asteroid.new @area, @position, Uni.random(-Math::PI..Math::PI), Uni.randomf(MIN_SIZE.to_f..@radius*0.75) if @radius*0.75 > MIN_SIZE
        end
      end
    end
    return
  end

end