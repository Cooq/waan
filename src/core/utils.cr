struct LibRay::Vector2
  property x, y
  def +(other_point : LibRay::Vector2)
    LibRay::Vector2.new(x: @x + other_point.x, y: @y + other_point.y)
  end
  def +(x : Number, y : Number)
    LibRay::Vector2.new(x: @x + x, y: @y + y)
  end
  def +(x : Number, y : Number, angle : Number)
    LibRay::Vector2.new(x: @x - Math.sin(angle) * (x + y) , y: @y + Math.cos(angle) * (x + y))
  end
  def /(a : Number)
    LibRay::Vector2.new(x: @x / a, y: @y / a)
  end
  def *(a : Number)
    LibRay::Vector2.new(x: @x * a, y: @y * a)
  end
  def rotate(aPivot : Vector2, rad : Float64) : Vector2
    @x -= aPivot.x
    @y -= aPivot.y
    s = Math.sin(rad);
    c = Math.cos(rad);
    return aPivot + Vector2.new(
        x: @x * c - @y * s,
        y: @y * c + @x * s
    );
  end
  def rotate(rad : Float64) : Vector2
    s = Math.sin(rad);
    c = Math.cos(rad);
    return Vector2.new(
        x: @x * c - @y * s,
        y: @y * c + @x * s
    );
  end
  def to_s(io : IO)
    io << @x.to_i << " " << @y.to_i
  end
end

# Unification of statics Helpers
class Uni
  # const
  WIDTH = 1920
  WIDTH_f = WIDTH.to_f
  HEIGHT = 1080
  HEIGHT_f = HEIGHT.to_f
  SPEED     = 600
  DRAW_DELTA= 0.008

  @@rnd = Random.new
  def self.random()
    @@rnd.rand()
  end
  def self.random(a : Range)
    @@rnd.rand(a)
  end
  def self.randomf(a : Range)
    @@rnd.rand(a).to_f
  end
  def self.random(max)
    @@rnd.rand(max)
  end
  def self.randomf(max)
    @@rnd.rand(max).to_f
  end
end