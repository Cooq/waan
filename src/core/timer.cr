class Timer

  @duration = 0.0
  @count = 0.0
  @functions = [] of Timer_informations

  def self.linear(s : Float64 | Int32); s end
  def self.quad  (s : Float64 | Int32); s*s end
  def self.cubic (s : Float64 | Int32); s*s*s end
  def self.quart (s : Float64 | Int32); s*s*s*s end
  def self.quint (s : Float64 | Int32); s*s*s*s*s end
  def self.sine  (s : Float64 | Int32); 1-math.cos(s*math.pi/2) end
  def self.expo  (s : Float64 | Int32); 2^(10*(s-1)) end
  def self.circ  (s : Float64 | Int32); 1 - math.sqrt(1-s*s) end
  def self.nothing; ->{}; end
  def self.nothing_d; ->(d : Float64){}; end

  def update(dt : Float64)
    @functions = @functions.map do |info|
      info.time += dt
      info.during.call(dt.to_f)
      if (info.time > info.limit)
        info.count -= 1
        info.time = 0.0
        info.after.call()
      end
      info
    end.select do |info|
      info.count > 0
    end
  end

  def after (s : Float64 | Int32, func : Proc(Void))
    @functions << Timer_informations.new(s, Timer.nothing_d, func, 1)
  end

  def during (s : Float64 | Int32, func : Proc(Float64, Nil))
    @functions << Timer_informations.new(s, func, Timer.nothing, 1)
  end

  def during (s : Float64 | Int32, func : Proc(Float64, Nil), f2 : Proc(Void))
    @functions << Timer_informations.new(s, func, f2, 1)
  end

  def every (s : Float64 | Int32, func : Proc(Void), count)
    @functions << Timer_informations.new(s, Timer.nothing_d, func, count)
  end

  def every (s : Float64 | Int32, func : Proc(Void))
    @functions << Timer_informations.new(s, Timer.nothing_d, func, Float64::INFINITY)
  end

  def tween (s : Float64 | Int32, from : Number, to : Number, func : Proc(Number, Nil))
    a = (from - to) / s
    @functions << Timer_informations.new(s, ->(dt : Float64){func.call(a*dt)}, Timer.nothing, 1)
  end

end

struct Timer_informations
  @time = 0.0
  property time
  property during
  property after
  property limit
  property count
  def initialize(@limit : Float64 | Int32, @during : Proc(Float64, Nil), @after : Proc(Nil), @count : Int32 | Float64); end
end
