require "./game_object"

class Area
  # @@log = Logger.new(STDOUT)
  @score = 0
  property score

  def initialize
    @game_objects = {} of String => GameObject
  end

  def update(dt)
    sz = @game_objects.size
    @game_objects.each do | key, obj |
      sz -=1
      break if sz < 0
      obj.update(dt)
      @game_objects.delete(key) if obj.dead?
    end
  end

  def draw
    @game_objects.each do | _, obj |
      obj.draw()
    end
  end

  def add_game_object(g)
    @game_objects[g.id] = g
  end

  def select(&block)
    @game_objects.select do |i, j| yield j end
  end

  def each(&block)
    sz = @game_objects.size
    @game_objects.each do |i, j|
      sz -= 1
      break if sz < 0
      yield j
    end
  end

  def [](s)
    return @game_objects[s]
  end

  def <<(s)
    @game_objects[s.id] = s
  end

  def next(sym : Symbol); end

  def die
    each do | obj | obj.die end
  end

end