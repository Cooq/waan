require "./timer"
require "./area"
require "./utils"

class GameObject
  @position = LibRay::Vector2.new(x: 0, y: 0)
  @life = 1.0
  @id : String
  @area = Pages.current.area || Area.new
  @collision : Nil | Symbol
  ID_LEN = 16
  property position, timer, collision
  getter life
  getter id
  getter area

  def initialize(@area : Area, @position : LibRay::Vector2)
    initialize(@area)
  end

  def initialize
    @timer = Timer.new
    @id = uuid
  end

  def initialize(@area : Area)
    initialize()
    @area << self
  end

  def draw
    LibRay.draw_text @position.to_s, @position.x, @position.y, 8, LibRay::WHITE if DEBUG
  end

  def update(dt)
    @timer.update(dt.to_f)
  end

  def area= (@area : Area)
    @area << self
  end

  def die : Void
    @life = 0.0
  end

  def dead? : Bool
    @life <= 0
  end

  def hit (damage) : Void
    @life -= damage
    die if dead?
  end

  def []
    @area.add_game_object self
  end

  protected def uuid : String
    str = "AQgwBRhxCSiyDTjzEUk0FVl1GWm2HXn3IYo4JZp5Kaq6Lbr7Mcs8Ndt9Oeu+Pfv/"
    n = ""
    ID_LEN.times do |_|
      n += str[Uni.random(str.size)]
    end
    return n
  end

  def to_s(io : IO)
    io << @id <<": "<< @x <<" "<< @y <<" - "<< @life
  end
end
