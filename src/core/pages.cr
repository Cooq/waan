require "./area"

class Pages
  @@current_page = Pages.new
  @@speed = 1.0
  # @@font = LibRay.load_sprite_font "assets/font/slkscre.ttf"
  # class_property font
  class_property current_page
  class_property speed
  getter area

  def initialize(@area : Area)
  end
  def initialize
    @area = Area.new
  end
  def draw
    @area.draw
  end
  def update(dt)
    @area.update(dt*@@speed)
  end
  def self.change(page)
    @@current_page = page
  end
  def self.new_and_set(area : Area)
    Pages.change Pages.new(area)
  end
  def set
    @@current_page = self
  end
  def next(sym : Symbol)
    @area.next(sym)
  end
  def self.current
    return @@current_page
  end
end