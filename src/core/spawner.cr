require "./game_object"
require "./utils"

class Spawner < GameObject

  @time = 0.0
  def initialize(@area : Area, @x_range : Range(Float64, Float64), @y_range : Range(Float64, Float64), @time_range : Range(Float64, Float64), @func : Proc(Float64, Float64, Nil))
    super(@area)
  end

  def reset
    @time = Uni.randomf(@time_range)
  end

  def update(dt)
    @time -= dt
    if @time < 0
      reset
      @func.call(Uni.randomf(@x_range), Uni.randomf(@y_range))
    end
  end
end