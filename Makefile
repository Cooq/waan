OUTPUT=build/main
FILENAME=src/main.cr

all: force
	crystal run $(FILENAME)

build: force
	crystal build $(FILENAME) -o $(OUTPUT)

force: