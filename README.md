# WAAN

## What does WAAN mean

Waan is the acronym of What An Awesome Name, because I did not have any inspiration

## I want to play

Sure do it.

## How to play

### Run

```sh
shard install
crystal run src/main
```

If you have the error `crystal-run-main.tmp: error while loading shared libraries: libraylib.so.2: cannot open shared object file: No such file or directory`, be sure you installed [raylib](https://github.com/raysan5/raylib) and did :

```sh
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/
export LD_LIBRARY_PATH
```

### Controls

- `ZQSD` (or WASD on QWERTY) to move
- `A` to change time speed
- `space` to fire at an amazing rate

## Todo list

- [ ] Story mode
  - [ ] Saves
  - [ ] More weapons
  - [ ] More vessels
  - [ ] Objects (health, shields, bombs, etc.)
- [ ] Pause

## Is it awesome

Sure.

## License
[Non-Profit Open Software License 3.0 (NPOSL-3.0)](https://tldrlegal.com/license/non-profit-open-software-license-3.0-(nposl-3.0))